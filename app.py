import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode


application = Flask(__name__)
app = application


def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
    return db, username, password, hostname


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = 'CREATE TABLE movies(id INT UNSIGNED NOT NULL AUTO_INCREMENT, ' \
                'title varchar(255), year INTEGER, director TEXT, actor TEXT, ' \
                'release_date TEXT, rating REAL, UNIQUE (title), PRIMARY KEY (id))'

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
        populate_data()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)


def populate_data():

    db, username, password, hostname = get_db_creds()

    print("Inside populate_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                       host=hostname,
                                       database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cur.execute("INSERT INTO movies(title, year, director, actor, release_date, rating) "
                "values ('test title', '2020', 'Rowdy', 'Cash', 'March 17th', '5')")
    cnx.commit()
    print("Returning from populate_data")


def query_data():

    db, username, password, hostname = get_db_creds()

    print("Inside query_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    cur.execute("SELECT title FROM movies")
    entries = [dict(movies=row[0]) for row in cur.fetchall()]
    return entries


try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table global")
    create_table()
    print("After create_data global")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None

# insert movie into db
@app.route('/add_movie', methods=['POST'])
def add_movie():
    print("Received movie request")
    director = request.form['director']
    rating = request.form['rating']
    title = request.form['title']
    release_date = request.form['release_date']
    year = request.form['year']
    actor = request.form['actor']

    db, username, password, hostname = get_db_creds()
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    sql = "INSERT INTO movies (title, year, director, actor, release_date, rating) values (%s, %s, %s, %s, %s, %s)"
    vals = (title, year, director, actor, release_date, rating)
    flash_message = []

    try:
        cur.execute(sql, vals)
        cnx.commit()
        flash_message.append("Movie {} successfully inserted".format(title))
    except Exception as exp:
        flash_message.append("Movie %s could not be inserted - %s" % (title, exp))
    return render_template('index.html', message=flash_message)

# update movie
@app.route('/update_movie', methods=['POST'])
def update_movie():
    print("Received movie request.")
    flash_message = []
    title = request.form['title']
    if title == '':
        flash_message.append('Movie title must be specified to update record.')
        return render_template('index.html', message=flash_message)

    db, username, password, hostname = get_db_creds()

    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    sql = "SELECT * FROM movies WHERE title = %s"
    titles = (title, )

    try:
        cur.execute(sql, titles)
        result = cur.fetchall()
        if not result:
            flash_message.append("Movie with %s does not exist" % title)
            return render_template('index.html', message=flash_message)
    except Exception as exp:
        flash_message.append(exp)
        return render_template('index.html', message=flash_message)
    record = result[0]

    for key in request.form.keys():
        if key != 'title' and request.form[key]:
            print(key)
            print(type(request.form[key]))
            sql = "UPDATE movies SET " + key + " = %s WHERE id = %s"
            vals = (request.form[key], record[0])
            try:
                cur.execute(sql, vals)
                cnx.commit()
            except Exception as exp:
                flash_message.append("Movie could not be updated - %s" % exp)
                return render_template('index.html', message=flash_message)

    flash_message.append("Movie {} successfully updated".format(title))
    return render_template('index.html', message=flash_message)

# delete movie with given title
@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    flash_message = []
    title = request.form['delete_title']
    if title == '':
        flash_message.append('Movie title must be specified to delete movie.')
        return render_template('index.html', message=flash_message)

    db, username, password, hostname = get_db_creds()

    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    sql = "SELECT * FROM movies WHERE title = %s"
    titles = (title, )

    try:
        cur.execute(sql, titles)
        result = cur.fetchall()
        if not result:
            flash_message.append("Movie with %s does not exist" % title)
            return render_template('index.html', message=flash_message)
    except Exception as exp:
        flash_message.append(exp)
        return render_template('index.html', message=flash_message)
    record = result[0]

    sql = "DELETE FROM movies WHERE id = %s"
    vals = (record[0], )

    try:
        cur.execute(sql, vals)
        cnx.commit()
    except Exception as exp:
        flash_message.append("Movie could not be deleted - %s" % exp)
        return render_template('index.html', message=flash_message)

    flash_message.append("Movie {} successfully deleted".format(title))
    return render_template('index.html', message=flash_message)

# search movies for a given actor
@app.route('/search_movie', methods=['POST'])
def search_movie():
    flash_message = []
    actor = request.form['search_actor']
    if actor == '':
        flash_message.append('Actor name must be specified')
        return render_template('index.html', message=flash_message)

    db, username, password, hostname = get_db_creds()

    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    sql = "SELECT * FROM movies WHERE actor = %s"
    act = (actor, )

    try:
        cur.execute(sql, act)
        result = cur.fetchall()
        if not result:
            flash_message.append("No movies found for actor %s " % actor)
            return render_template('index.html', message=flash_message)

    except Exception as exp:
        flash_message.append(exp)
        return render_template('index.html', message=flash_message)

    for movie in result:
        flash_message.append("%s, %s, %s" % (movie[1], movie[2], movie[4]))
    return render_template('index.html', message=flash_message)


# grab movie(s) with highest rating
@app.route('/highest_rating', methods=['GET'])
def highest_rating():
    return get_stat_movie(True)


# grab movie(s) with lowest rating
@app.route('/lowest_rating', methods=['GET'])
def lowest_rating():
    return get_stat_movie(False)


# enter True to get highest movie, false to get lowest movie
def get_stat_movie(high):
    flash_message = []

    db, username, password, hostname = get_db_creds()

    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    if high:
        sql = "SELECT * FROM movies WHERE rating = (SELECT MAX(rating) FROM movies)"
    else:
        sql = "SELECT * FROM movies WHERE rating = (SELECT MIN(rating) FROM movies)"


    try:
        cur.execute(sql)
        result = cur.fetchall()
        if not result:
            flash_message.append("No movies found")
            return render_template('index.html', message=flash_message)
    except Exception as exp:
        flash_message.append(exp)
        return render_template('index.html', message=flash_message)

    for movie in result:
        flash_message.append("%s, %s, %s, %s, %s" % (movie[1], movie[2], movie[4], movie[3], movie[6]))
    return render_template('index.html', message=flash_message)


@app.route("/")
def hello():
    print("Inside hello")
    print("Printing available environment variables")
    print(os.environ)
    print("Before displaying index.html")
    entries = query_data()
    print("Entries: %s" % entries)
    return render_template('index.html', entries=entries)


if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')

